# Problem

Managing bookmarks is hard.

# Solution

bookmarks-manager makes it easy to
* create
* manage
* access, and
* share
bookmarks.

# Features

* a bookmark data structure
* a bookmark management interface
* a web interface
* a social mechanism for search, sharing, and discovery

